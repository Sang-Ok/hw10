#include "grader.h"

int main() {
  vector<Subject*> subjects;		//tout<<"start program"<<endl;
  string cmd;
  for (cin >> cmd; cmd == "subject"; cin >> cmd) {
    string name, type;
    int credit, a,b,c,d;
    Subject* new_sub;
    cin >> name >> credit >> type >> a;
    if (type == "PF") { //new를 하면 constructer가 안동작하는데 어쩌지. //어라? 최신 C는 new에서도 constructer parameter 받나?
      new_sub= new SubjectPassFail(name, credit, a);
    } else if( type == "G" ) {
      cin >> b >> c >> d;
      new_sub= new SubjectGrade(name, credit, a, b, c, d);
    } else if( type == "G+" ) {
      cin >> b >> c >> d;
      new_sub= new SubjectGradePlus(name, credit, a, b, c, d);
    }
    subjects.push_back(new_sub);
  }

  const int subject_num = subjects.size();  
  int total_credit=0;
  for(int i=0; i < subject_num; i++) total_credit += subjects[i]->credit();
  vector<pair<string, double> > student_grades;

  for (; cmd != "quit"; cin >> cmd) {
    if (cmd == "eval") {
      string name;
      string grades[subject_num];
      double score_sum = 0;
      cin >> name;
      for(int i=0; i < subject_num; i++){
        int score;
        cin >> score;
        grades[i] = subjects[i]->GetGrade(score);
        score_sum += GetNumberGrade(grades[i]) * subjects[i]->credit();
      }
      const double average_grade = score_sum / total_credit;
      student_grades.push_back( make_pair(name, average_grade) );

      cout << "      \t" << name;
      for(int i=0; i < subject_num; i++){
        cout << '\t' << grades[i];
      }
      cout << endl;
    }
  }
  
  sort(student_grades.begin(), student_grades.end(), CompareStudent);

  for (int i = 0; i < student_grades.size(); ++i) {
    // 여기에서 학점 출력이 소숫점 두자리만 되도록 cout의 멤버함수 호출
    cout << student_grades[i].first << " "
         << setprecision(3) << student_grades[i].second << endl;
  }
  return 0;
}




















