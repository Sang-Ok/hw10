#ifndef _CALANDAR_H_
#define _CALANDAR_H_
#include <set>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <ostream>

extern std::set<int> DATA_FOR_MONTH31;
extern std::set<int>& month31;
using namespace std;
//using std::ofstream;

class Date {
public:
 Date(int year=0, int month=1, int day=1) : year_(year), month_(month), day_(day){}
 void NextDay(int n = 1);
 bool SetDate(int year, int month, int day);
 int year() const{return year_;}
 int month() const{return month_;}
 int day() const{return day_;}
private:
 // 윤년을 판단하여 주어진 연도에 해당하는 날짜 수(365 또는 366)를 리턴.
 static int GetDaysInYear(int year);
 static int GetDaysInMonth(int year, int month);
 // 해당 날짜가 해당 연도의 처음(1월 1일)부터 며칠째인지를 계산.
 static int ComputeDaysFromYearStart(int year, int month, int day);
 static bool SettableDate(int year, int month, int day);
 int year_, month_, day_;
};
struct InvalidDateException {
 InvalidDateException(const std::string& str) : input_date(str) {}
 std::string input_date;
};

// yyyy.mm.dd 형식으로 입출력.
// 사용자 입력 오류시 >> operator는 InvalidDateException을 발생할 수 있음.

istream& operator>>(istream& is, Date& c);
ostream& operator<<(ostream& os, const Date& c);
void Initialize();





#endif
