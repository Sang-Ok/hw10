//bank_account.cc
#include "bank_account.h"
#include <fstream>
#include <ostream>
    
unsigned int Account::ComputeExpectedBalance(unsigned int n_years_later) const{
  return balance_ + interest_ * n_years_later;
}
      
unsigned int SavingAccount::ComputeExpectedBalance(
    unsigned int n_years_later) const{
  double ret = balance_;
  for(int i=0; i < n_years_later; i++)
    ret *= (1 + interest_rate_);
  return static_cast<int>(ret);
}
    
      
Account* CreateAccount(const string& type,
    const string& name, unsigned int balance, double interest_rate){
  Account* ret;
  if(type == "checking")
    ret = new Account(name, balance, interest_rate);
  else if (type == "saving")
    ret = new SavingAccount(name, balance, interest_rate);
  return ret;
}


void ShowAccounts(const vector<Account*>& accounts,
                  const unsigned int n_years_later){
  vector<Account*>::const_iterator i;
  for (i=accounts.begin(); i != accounts.end(); i++){
    const Account& a = **i;
    cout << a.name() << '\t'
         << a.type() << '\t' 
         << a.ComputeExpectedBalance(n_years_later) << '\t'
         << a.interest_rate() << endl;
  }
}
void DeleteAccount(vector<Account*>& accounts, string name){
  vector<Account*>::iterator i;
  for (i=accounts.begin(); i != accounts.end(); i++)
    if ((*i)->name() == name) break;
  if(i != accounts.end()){
      delete *i;
      accounts.erase(i);
  }
}
bool SaveAccounts(const vector<Account*>& accounts, const string& filename){
  ofstream os;
  os.open(filename.c_str());// how to [return false]?
  string name, type;
  unsigned int balance;
  double interest_rate;
  for (vector<Account*>::const_iterator i = accounts.begin(); i != accounts.end(); i++) {
    Account& a = **i;
	os << a.name() << ' ' << a.type() << ' ' << a.balance() << ' ' << a.interest_rate() << endl;
  }
  os.close();
  return true;
}
bool LoadAccounts(const string& filename, vector<Account*>& accounts){
  ifstream is;
  is.open(filename.c_str());// how to [return false]?
  string name, type;
  unsigned int balance;
  double interest_rate;
  while(is.good()){
    is >> name >> type >> balance >> interest_rate;
    accounts.push_back(CreateAccount(type, name, balance, interest_rate));
  }
  return true;
}

