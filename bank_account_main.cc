//bank_account_main.cc
#include "bank_account.h"



int main(){
  vector<Account*> accounts;
  string cmd;
  while(cmd != "quit"){
    cin >> cmd;
    try{
      if (cmd == "add"){
        string name, type;
        unsigned int balance;
        double interest_rate;
        cin >> name >> type >> balance >> interest_rate;
        accounts.push_back(CreateAccount(type, name, balance, interest_rate));
      } else if (cmd == "show"){
        ShowAccounts(accounts);
      } else if (cmd == "after"){
        int n_year_after;
        cin >> n_year_after;
        ShowAccounts(accounts, n_year_after);
      } else if (cmd == "delete"){
        string name;
        cin >> name;
        DeleteAccount(accounts, name);
      } else if (cmd == "load"){
        string filename;
        cin >> filename;
        if (LoadAccounts(filename, accounts) == false)
          InvalidAccountError message="Can't find file:: " + filename;
      } else if (cmd == "save"){
        string filename;
        cin >> filename;
        if (SaveAccounts(accounts, filename) ==false)
          InvalidAccountError message="Can't find file:: " + filename;
      }
    }catch(InvalidAccountError message){
	  cout << message << endl;
    }
  }
  for(int i=0; i < accounts.size(); i++) delete accounts[i];
  return 0;
}