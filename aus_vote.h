#ifndef _AUS_VOTE_H_
#define _AUS_VOTE_H_
#include <string>
#include <vector>
#include <iostream>

using namespace std;
#define tout cout<<"\t\tTEST::"

struct Candidate {
  std::string name;
  int votes;
};

typedef vector<Candidate> RoundResult;

class AusVoteSystem {
public:
  AusVoteSystem(const vector<string>& candidate_names)
  : candidate_names_(candidate_names){}
  bool AddVote(const vector<int>& vote);
  typedef vector<Candidate> RoundResult;
  vector<RoundResult> ComputeResult() const;
private:
  vector<string> candidate_names_;
  vector<vector<int> > votes_;
};


ostream& operator << (ostream& os, vector<RoundResult> result);



#endif
