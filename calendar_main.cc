//calandar_main.cc
#include "calendar.h"
#include <vector>
#include <cstdlib>
using namespace std;

std::set<int>& month31 = DATA_FOR_MONTH31;


int main() {
  Initialize();
  Date calandar;
  string cmd;

  while (cmd != "quit") {
    cin >> cmd;
    try {
      if (cmd.substr(0,4) == "next") {
        int moreday;
        if (cmd == "next_day")  moreday=1;
        else if (cmd == "next") cin >> moreday;
        else continue;
        calandar.NextDay(moreday);
      } else if (cmd == "set") {
        cin >> calandar;
      }
      cout << calandar << endl;
    } catch (InvalidDateException& e) {
      cout << "Invalid Date: " << e.input_date << endl;
    }
  }
  
  return 0;
}


