#ifndef _GR_H_
#define _GR_H_

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <iomanip>


using namespace std;


#define tout cout<<"\t\tTEST::"


// 0~100점의 성적을 학점으로 변환하는 클래스. 과목 이름과 시수를 저장.
class Subject {
public:
  Subject(const string& name, int credit) : name_(name), credit_(credit) {}
  virtual ~Subject() {}
  const string& name() const { return name_; }
  int credit() const { return credit_; }
  virtual string GetGrade(int score) const = 0;
private:
  string name_;
  int credit_;
};

// 성적을 Pass / Fail 로 구분하여 출력해주는 클래스.
// 성적이 pass_score보다 같거나 높으면 "P", 아니면 "F"를 리턴.
class SubjectPassFail : public Subject {
public:
  SubjectPassFail(const string& name, int credit, int pass_score)
	 : Subject(name, credit), pass_score_(pass_score){}

  virtual ~SubjectPassFail(){}
  virtual string GetGrade(int score) const{
    if(score >= pass_score_) return "P";
    else return "F";
  }
protected:
  string name;
  int pass_score_;
};

// 성적을 A, B, C, D, F 로 구분하여 출력해주는 클래스.
// 성적이 속하는 구간에 따라
// 100 >= "A" >= cutA > "B" >= cutB > "C" >= cutC > "D" >= cutD > "F".
class SubjectGrade : public Subject {
public:
  SubjectGrade(const string& name, int credit,
              int cutA, int cutB, int cutC, int cutD)
   : Subject(name, credit), cutA_(cutA), cutB_(cutB),cutC_(cutC),cutD_(cutD){}


  virtual ~SubjectGrade(){}
  virtual string GetGrade(int score) const{
    if(score > 100) return "ERROR";
    else if(score >= cutA_) return "A";
    else if(score >= cutB_) return "B";
    else if(score >= cutC_) return "C";
    else if(score >= cutD_) return "D";
    else return "F";
  }
protected:
  int cutA_, cutB_, cutC_, cutD_;
};

class SubjectGradePlus : public SubjectGrade{
public:
  SubjectGradePlus(const string& name, int credit,
              int cutA, int cutB, int cutC, int cutD)
    : SubjectGrade(name, credit, cutA, cutB, cutC, cutD){}
  virtual ~SubjectGradePlus(){}
  virtual string GetGrade(int score) const{
    const int cutAp = (3 * cutA_ - cutB_)/2,
              cutBp = (cutA_ + cutB_)/2,
              cutCp = (cutB_ + cutC_)/2,
              cutDp = (cutC_ + cutD_)/2;
              
    if(score > 100) return "ERROR";
    else if(score >= cutAp) return "A+";
    else if(score >= cutA_) return "A";
    else if(score >= cutBp) return "B+";
    else if(score >= cutB_) return "B";
    else if(score >= cutCp) return "C+";
    else if(score >= cutC_) return "C";
    else if(score >= cutDp) return "D+";
    else if(score >= cutD_) return "D";
    else return "F";
  }
};


double GetNumberGrade(const string& str);
bool CompareStudent(const pair<string, double>& a,
                    const pair<string, double>& b);

#endif


































