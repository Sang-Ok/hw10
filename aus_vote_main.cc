//aus_vote_main.cc
#include "aus_vote.h"

int main() {
  int num_c, num_v, temp;
  vector<string> candidat_names;
  cin >> num_c;
  for(int i=0; i < num_c; i++){
  string name;
  cin >> name;
  candidat_names.push_back(name);
  }
  AusVoteSystem sys(candidat_names);
  
  cin >> num_v;
  for(int j=1; j <= num_v; j++){
    vector<int> vote;
    vote.resize(num_c,-999);
    for(int i=0; i < num_c; i++){
      cin >> temp;
      if(0 < temp && temp <= num_c) vote[temp-1] = i; //표에 적은대로 받는 게 아니라 우선순위 높은 후보부터 앞세움
      else break;
    }
  sys.AddVote(vote);
  }
  cout << sys.ComputeResult() << endl;
}
