#ifndef _BA_H_
#define _BA_H_
#include <string>
#include <vector>
#include <iostream>
//grader과제 subject들 delete해야됨

using namespace std;
typedef string 	InvalidAccountError;

class Account { 
public:
  Account(const string& name, unsigned int balance, double interest_rate)
    : name_(name), balance_(balance), interest_rate_(interest_rate),
    interest_(balance * interest_rate){}
  virtual ~Account(){}
  
  void Deposit(unsigned int amount){ balance_ += amount;}
  bool Withdraw(unsigned int amount){balance_ -= amount;}
        
  virtual unsigned int ComputeExpectedBalance(
      unsigned int n_years_later) const;
      
  virtual const char* type() const { return "checking"; }
  const unsigned int& balance() const { return balance_; }
  const string& name() const { return name_; }
  const double& interest_rate() const { return interest_rate_; }
        
protected:
  string name_;
  unsigned int balance_;
  double interest_rate_;  // 기본 계좌는 단리로 계산.
  double interest_;
};           
             
class SavingAccount : public Account {
public:
  SavingAccount(const string& name, int balance, double interest_rate)
    : Account(name, balance, interest_rate){}
  virtual ~SavingAccount(){}
      
  virtual const char* type() const { return "saving"; }
  // 이 타입의 계좌는 복리로 계산.
  virtual unsigned int ComputeExpectedBalance(
      unsigned int n_years_later) const;
};    
      
Account* CreateAccount(const string& type,
    const string& name, unsigned int balance, double interest_rate);
void ShowAccounts(const vector<Account*>& accounts,
	              const unsigned int n_years_later=0);
void DeleteAccount(const vector<Account*>& accounts, string name);
bool SaveAccounts(const vector<Account*>& accounts, const string& filename);
bool LoadAccounts(const string& filename, vector<Account*>& accounts);

/* 필요시 private 멤버 변수 및 함수 추가 가능.

설계 시 고려사항
계좌의 종류에 따라 이자는 단리 또는 복리로 계산한다 (이자는 연 단위 이자이다.)
ex)  100원을 입금하고 단리 10%이자율로 10년이 지나면 200원이 된다. 복리 10%라면 259원 가량이 된다. (계산을 한 후, 소수 첫번째 자리에서 버림)
ex) 100원을 입금하고 복리 10% 이자율로 7년이 지나면 194.87171원이 된다. 따라서 화면에는 194가 출력된다.
add <이름> <종류> <금액> <이자율> : 정해진 종류의 계좌를 추가한다. 
종류 : checking, saving
checking : 해당 고객의 이자는 단리로 계산
saving : 해당 고객의 이자는 복리로 계산
delete <이름> : 계좌를 삭제한다.
show : 계좌 정보를 모두 출력.
after <n_years> : 주어진 기간 후 예상 금액을 출력.
save <file_name> : 주어진 이름의 파일로 모든 정보 저장.
load <file_name> : 주어진 이름의 파일에서 정보를 읽어들임.
quit : 프로그램 종료
*/
#endif