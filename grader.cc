#include "grader.h"

bool CompareStudent(const pair<string, double>& a,
                           const pair<string, double>& b) {
  return (a.second > b.second);
}

double GetNumberGrade(const string& str) {
  if (str == "A" || str == "P") return 4.0;
  if (str == "B") return 3.0;
  if (str == "C") return 2.0;
  if (str == "D") return 1.0;
  if (str == "A+") return 4.5;
  if (str == "B+") return 3.5;
  if (str == "C+") return 2.5;
  if (str == "D+") return 1.5;
  return 0.0;
}


