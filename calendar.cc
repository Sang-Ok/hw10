//calendar.cc
#include "calendar.h"
using namespace std;

std::set<int> DATA_FOR_MONTH31;


string itostr(int i) {
  string ret;
  vector<char> v;
  for(;0<i;i/=10) v.push_back('0'+i%10);
  for(i=v.size()-1; 0<=i; i--) ret+=v[i];
  if (ret == "" ) ret = "0";
  return ret;
}

void Initialize() {
  DATA_FOR_MONTH31.insert(1);
  DATA_FOR_MONTH31.insert(3);
  DATA_FOR_MONTH31.insert(5);
  DATA_FOR_MONTH31.insert(7);
  DATA_FOR_MONTH31.insert(8);
  DATA_FOR_MONTH31.insert(10);
  DATA_FOR_MONTH31.insert(12);
}

static inline int abs(const int& n) {
  if (0 <= n) return n;
  else return -n;
}
static inline int hat(const int& n) { return n/abs(n);}

void Date::NextDay(int n ) {
  n += ComputeDaysFromYearStart(year_, month_, day_);
  month_ = day_ =1;

  const int NextYear = n / 365;
  n %= 365;
  if (NextYear != 0) {
    int leap=0;
    for (int i=0; abs(i) < abs(NextYear); i+= hat(NextYear) )
      if (366 == GetDaysInYear(i+year_))  leap++;
    leap *= hat(NextYear);
    year_ += NextYear;

    NextDay(n -leap);
    return;    //연변동 완료 (단, -365<n<0인경우는 아직 안됨)
  } else if (n < 0) {
    year_--;
    n += GetDaysInYear(year_);
  }//연변동 완료, 모든 경우에 대해. 0<=n<365(or 366)

  int m=1,d=1;
  for(; m <= 12; m++)
    if(n < ComputeDaysFromYearStart(year_,m+1,d)) break;
  for(; d <= GetDaysInMonth(year_,m); d++)
    if(n < ComputeDaysFromYearStart(year_,m,d+1)) break;
  month_ = m;
  day_ = d;
}

bool Date::SettableDate(int year, int month, int day) {
  if(month < 1 || 12 < month || day < 1 || GetDaysInMonth(year,month) < day) return false;
  else return true;
}

bool Date::SetDate(int year, int month, int day) {
  if(SettableDate(year, month, day) ) {
    year_ = year;
    month_= month;
    day_ = day;
    return true;
  }
  else return false;
}


int Date::GetDaysInMonth(int year, int month) {
  if(month < 1 || 12 < month)		return 0;
  else if (0 < month31.count(month))	return 31;
  else if (month != 2)			return 30;
  else if (GetDaysInYear(year) == 365 )	return 28;
  else					return 29;
}

int Date::GetDaysInYear(int year) {
  bool leap = false;
  if (year % 4 == 0) {
    leap = true;
    if (year % 100 ==0) {
      leap = false;
      if (year % 400 ==0) {
        leap = true;
      }
    }
  }
  if (leap) return 366;
  else return 365;
}

int Date::ComputeDaysFromYearStart(int year, int month, int day) {
  int ret=0;
  for (int m=1; m < month; m++) {
    ret+=GetDaysInMonth(year,m);
  }
  ret += day-1;
  return ret;
}


ostream& operator<<(ostream& os, const Date& c) {
  return os << c.year() << '.' << c.month() << '.' << c.day();
}

istream& operator>>(istream& is, Date& c) {
  int y,m,d;
  char a1,a2;
  is >> y >> a1 >> m >> a2 >> d;
  if ('.' != a1 || '.' != a2 || c.SetDate(y,m,d) == false)
    throw(InvalidDateException(itostr(y) + a1 + itostr(m) + a2 + itostr(d)));
  return is;
}





