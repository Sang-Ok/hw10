//aus_vote.cc
#include "aus_vote.h"

static void DeleteCode(const int& code, vector<int>& vec){
  for (int i = 0; i < vec.size(); i++) {
  if (vec[i] == code){
    vec.erase(vec.begin() + i);
    i--;
  }
  }
}

bool AusVoteSystem::AddVote(const vector<int>& vote){ 
 if (candidate_names_.size() != vote.size() )
    return false;
  int i,j;
  for (i = 0; i < vote.size(); i++){
    for (j = i+1; j < vote.size(); j++)
	  if(vote[i] == vote[j])
        return false;
    if (vote[i] < 0 || candidate_names_.size() <= vote[i])
      return false;
  }
  if (i < vote.size())
    return false;
  else{
    votes_.push_back(vote);
    return true;
  }
}


//typedef vector<Candidate> RoundResult;
vector<RoundResult> AusVoteSystem::ComputeResult() const{
  const vector<string>& names = candidate_names_;
  vector<vector<int> > v = votes_;
  vector<int> candidate_codes;
  vector<RoundResult> ret;

  candidate_codes.resize(names.size());

  for (int i = 0; i < names.size(); i++){
    candidate_codes[i] = i;
  }

  while (0 < candidate_codes.size()){
    RoundResult round;
    for (int i = 0; i < candidate_codes.size(); i++){
      const int& code = candidate_codes[i];
      int vote_for_him = 0;
      for (int j = 0; j < v.size(); j++){ 
      if (v[j][0] == code)
        vote_for_him++;
      }
      Candidate c;
      c.name = names[code];
      c.votes= vote_for_him;
      round.push_back(c);
    }
    ret.push_back(round);//라운드 입력 완료. 
    
    for(int i=0; i<round.size(); i++)
      if(static_cast<float>(votes_.size() )/2 < round[i].votes)
        return ret;  //과반수면 종료

    // 떨굴사람 찾기 시작
    Candidate worst = round[0];
    vector<string> kill_list;
    kill_list.push_back(round[0].name);
    for (int i=1; i< round.size(); i++){
      if (round[i].votes < worst.votes) {
        worst = round[i];
        kill_list.clear();
        kill_list.push_back(round[i].name);
      } else if(round[i].votes ==worst.votes) {
        kill_list.push_back(round[i].name);
    }
    }	//kill_list가 떨굴놈들 명단. 이제 이걸 코드로 변환함

    vector<int>kill_code;
    for (int i =0; i < kill_list.size(); i++){
      for (int j =0; j < candidate_codes.size(); j++){
        if(kill_list[i] == names[candidate_codes[j]])
        kill_code.push_back(candidate_codes[j]);
      }
    }	//변환완료. 이제 이거랑 관련된 모든 코드를 제거함.

    for(int i = 0; i < kill_code.size(); i++){
      for (int j =0; j < v.size(); j++){
        DeleteCode(kill_code[i], v[j]);
      }
      DeleteCode(kill_code[i], candidate_codes);
    }
  }	//생각해보니까 코드만들지 말고 그냥 이름가지고 처리할 걸 괜히 고생했네
  return ret;
}


//typedef vector<Candidate> RoundResult;
ostream& operator <<(ostream& os, RoundResult result){
  for (int i=0; i < result.size(); i++)
    os << result[i].name << ' ' << result[i].votes << ' ';
  return os;
}

ostream& operator <<(ostream& os, vector<RoundResult> result){
  int i;
  for (i=0; i < result.size(); i++) {
    os << "Round " << i+1 << ": " << result[i] << endl;
  }
  return os << "Winner : " << (result[i-1])[0].name;
}







